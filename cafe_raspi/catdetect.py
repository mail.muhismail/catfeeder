import cv2
import numpy as np
import os
import requests

from picamera import PiCamera
from picamera.array import PiRGBArray


APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
CLASSIFIER_DIR = os.path.join(APP_DIR, 'classifiers')
RECOGNIZER_DIR = os.path.join(APP_DIR, 'recognizers')

FRAME_WIDTH = 640
FRAME_HEIGHT = 480

HOST = 'http://192.168.8.101:5000'
DEVICE_ID = 'cat4269'

RECOGNIZER_URL = '{}/{}/{}/{}'.format(HOST, 'user', 'trainer', DEVICE_ID)


def detect_cat(image):
    
    cat_cascade = cv2.CascadeClassifier(
        os.path.join(CLASSIFIER_DIR, 'haarcascade_frontalcatface_extended.xml'))
    
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    cat_faces = cat_cascade.detectMultiScale(gray, 1.2, 3)
    
    for (x, y, w, h) in cat_faces:
        return True, gray[y:y+h, x:x+w]
    
    return False, ()


def recognize_cat(gray_image, recognizer_file):
    
    recognizer = cv2.face.LBPHFaceRecognizer_create()
    recognizer.read(os.path.join(RECOGNIZER_DIR, recognizer_file))

    id, confidence = recognizer.predict(gray_image)

    if confidence < 100:
        return True, (id, confidence)
    else:
        return False, (id, confidence)

    
def load_recognizer(url, filename):
    """ Load recognizer file (training result) from server

    :param url       String url to get recognizer file http://HOST_IP/user/trainer/<device_id>
    :param filename  String filename of recognizer file
    """
    req = requests.get(url)

    if req:
        with open(os.path.join(RECOGNIZER_DIR, filename), 'wb') as f:
            f.write(req.content)
        return True
    else:
        return False


def main():

    camera = PiCamera()
    camera.resolution = (FRAME_WIDTH, FRAME_HEIGHT)
    camera.framerate = 32

    rawCapture = PiRGBArray(camera, size=(FRAME_WIDTH, FRAME_HEIGHT))

    load_rec = load_recognizer(RECOGNIZER_URL, 'recognnizer.yml')

    if not load_rec:
        print('Failed to load recognizer')
        return False

    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        cat_detected, cat_faces = detect_cat(frame.array)

        if cat_detected:
            ret, data = recognize_cat(cat_faces, 'recognizer.yml')
            if ret:
                print('{} _ {}'.format(data[0], data[1]))
        else:
            print('Gaada Kucing')        

        cv2.imshow('frame',frame.array)

        rawCapture.truncate(0)  
    
        if cv2.waitKey(20) & 0xFF == ord('q'):
            break
    
    cv2.destroyAllWindows()
    

if __name__ == '__main__':
    main()
