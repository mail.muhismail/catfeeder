import cv2
import numpy as np
import os

APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
IMAGES_DIR = os.path.join(PROJECT_ROOT, 'uploads')
RECOGNIZER_DIR = os.path.join(PROJECT_ROOT, 'recognizer')

def detect_save_cat(path, src, media_type):
    """ Detect cat's face in the image and then save the image containing only the 
    cat's face to the path. It will return True if there's cat in the image and False
    if there isn't.

    :param path     String of path (including filename and extension) where the image
                    will be saved
    :param image    FileStorage image that will be checked if
    """
    
    if media_type is 'video':
        # Garbage and complicated but useful variable
        path_split = os.path.split(path)
        filename_and_ext = path_split[1].split('.')
        filename_split = filename_and_ext[0].rsplit('_', 1)
        img_id = int(filename_split[1])

        # Save video as temporary file. Why? Because i don't know how to read directly from FileStorage object.
        # Whenever I pass src.read() to save_cat(), there's always error about type. What I discovered is that
        # src.read() return byte object and save_cat() expecting image with array type.
        path_to_temp_video = os.path.join(path_split[0], 'temp', filename_and_ext[0] + '.' + filename_and_ext[1])
        src.save(path_to_temp_video)

        # Open with opencv
        video = cv2.VideoCapture(path_to_temp_video)

        while video.isOpened():
            ret, frame = video.read()
            
            if not ret: break
            
            # Filename <user_id>_<cat_id>_<img_id>.jpg
            filename = '{}_{}.{}'.format(filename_split[0], img_id, 'jpg')
            path = os.path.join(path_split[0], filename)
                        
            save_image = save_cat(path, frame, True)
            
            if save_image:
                img_id = img_id + 1
        
        video.release()
        
        return True
    
    return save_cat(path, src.read())


def save_cat(path, image, from_video=False):
    cat_cascade = cv2.CascadeClassifier(
        os.path.join(APP_DIR, 
        'classifier/haarcascade_frontalcatface_extended.xml'))
    
    if from_video:
        img = image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    else:
        img = cv2.imdecode(np.fromstring(image, np.uint8), cv2.IMREAD_UNCHANGED)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    cat_faces = cat_cascade.detectMultiScale(gray, 1.2, 3)
    
    for (x, y, w, h) in cat_faces:
        # Just get the first detected object/cat and fkng save it!
        cv2.imwrite(path, img[y:y+h, x:x+w])
        return True
    
    # It means nothing detected. I know, it's stupid.
    return False


def get_users_cat_images(user_id):

    # Get only all images of this user's cats.
    images_name = [img for img in os.listdir(IMAGES_DIR) if int(img.split('_')[0]) == user_id]

    cat_images = []
    cat_ids = []
    for img_name in images_name:
        # Read images and convert it to grayscale
        image_read = cv2.imread(os.path.join(IMAGES_DIR, img_name), cv2.IMREAD_GRAYSCALE)
        # Convert to numpy array
        image_np = np.array(image_read, 'uint8')

        cat_id = int(img_name.split('_')[1])

        cat_ids.append(cat_id)
        cat_images.append(image_np)

    return cat_ids, cat_images


def cat_training(user_id):

    recognizer = cv2.face.LBPHFaceRecognizer_create()
    cat_ids, cat_images = get_users_cat_images(user_id)

    if not cat_images or not cat_ids:
        return False

    print(cat_ids)
    recognizer.train(np.asarray(cat_images), np.array(cat_ids))
    recognizer.write(os.path.join(RECOGNIZER_DIR, 'recognizer_{}.yml'.format(user_id)))

    return True


