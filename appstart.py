""" Create app instance """

from cafe_api.application import create_app
from cafe_api.settings import DevConfig

app = create_app(DevConfig)


