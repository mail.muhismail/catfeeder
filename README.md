# Cat Feeder

Aplikasi ini dibuat untuk sistem pemberi makan kucing otomatis menggunakan cat recognition. Aplikasi yang terdapat pada repositori ini di-*install* pada server dan juga pada Raspberry Pi. Untuk server, aplikasi yang akan digunakan terdapat pada folder `cafe_api` dan `cafe_detection`, sedangkan untuk Raspberry Pi aplikasi terdapat pada folder `cafe_raspi`.

## Server
Server digunakan sebagai tempat penyimpanan data-data mengenai kucing pengguna. Selain itu, server juga dapat digunakan sebagai tempat *training* untuk aplikasi *cat recognizer* dengan menggunakan foto atau video yang didalamnya terdapat wajah kucing milik pengguna.

Modul `cafe_api` merupakan program API untuk melakukan *interfacing* antara *client* (alat pemberi makan kucing atau Raspberry Pi) dan *server* melalui HTTP. *Client* dapat melakukan pengolahan data kucing, seperti menambah, mengubah, atau menghapus data kucing. API ini juga memfasilitasi peng-*upload*-an foto atau video kucing pengguna yang nantinya akan digunakan untuk proses *training* data. Hasil upload akan disimpan pada folder `uploads`. Proses training data tersebut dilakukan dengan menggunakan modul `cafe_detection`. Hasil *training* kemudian akan disimpan pada folder `recognizer`.

Program API dibuat dengan menggunakan [Flask](http://flask.pocoo.org/) dan juga beberapa *library* lainnya (lihat file: `requirements.txt`). Sedangkan program untuk proses *training* digunakan [OpenCV](https://opencv.org/).

Struktur File pada server:
```
project_root
|-- cafe_api
|-- cafe_detection
|-- recognizer*
|-- uploads*
|   |-- temp*
|-- appstart.py
|-- requirements.txt
```

Install dengan menggunakan `pip install -r requirements.txt` pada *project root*.

**Note**:
\* Buat folder sebelum aplikasi dijalankan

## Raspberry Pi
Program yang dipasangkan pada Raspberry Pi terdapat pada folder `cafe_raspi`. Program ini digunakan untuk melakukan proses pengenalan kucing dan pendeteksian kucing yang dilakukan oleh alat ***Cat Feeder***. Program ini juga akan melakukan *request* file hasil *training* dan kemudian menyimpannya secara lokal.

Struktur File pada Raspberry Pi:
```
cafe_raspi
|-- classifier
|-- recognizer
|-- catdetect.py
```

Install [Python](https://www.python.org/), [OpenCV](https://opencv.org/), [numpy](www.numpy.org/), dan [requests](docs.python-requests.org/en/master/) pada Raspberry Pi. Numpy dan Requests dapat diinstall dengan menggunakan `pip`.

Jalankan file `catdetect.py`