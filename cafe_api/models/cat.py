from cafe_api.extensions import db

class Cat(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    food_ration = db.Column(db.Float, nullable=False, default=0.0)
    schedules = db.relationship('Schedule', backref='cat', lazy=True)
    # Defines relation the the user. Whose cat it is?
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __init__(self, name, food_ration):
        
        self.name = name
        self.food_ration = food_ration


class Schedule(db.Model):
    
    """ Eat schedule for cat to obey.
    """
    
    id = db.Column(db.Integer, primary_key=True)
    food_ration = db.Column(db.Float, nullable=True)
    time_start = db.Column(db.Time, nullable=False)
    time_end = db.Column(db.Time, nullable=False)
    cat_id = db.Column(db.Integer, db.ForeignKey('cat.id'), nullable=False)

    def __init__(self, time_start, time_end, food_ration=None):
        
        self.food_ration = food_ration
        self.time_start = time_start
        self.time_end = time_end