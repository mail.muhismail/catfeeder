
from cafe_api.extensions import db, bcrypt

class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)
    first_name = db.Column(db.String(80), nullable=False)
    last_name = db.Column(db.String(80), nullable=False)
    device_id = db.Column(db.String(10), unique=True, nullable=True)
    cats = db.relationship('Cat', backref='user', lazy=True)

    def __init__(self, username, password, first_name, last_name, devide_id=None):
        self.username = username
        self.password = self.hash_password(password)
        self.first_name = first_name
        self.last_name = last_name
        self.device_id = None
    
    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def hash_password(self, password):
        return bcrypt.generate_password_hash(password).decode('utf-8')
