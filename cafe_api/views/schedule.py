from datetime import datetime

from flask import Blueprint, request, jsonify
from flask_jwt_simple import jwt_required, get_jwt_identity

from cafe_api.extensions import db, jwt
from cafe_api.models import Schedule, Cat
from cafe_api.schemas import CatSchema, ScheduleSchema
from cafe_api.views.user import user_loader
from cafe_api.views.cat import user_cat_validation

bp_sch = Blueprint('schedule', __name__, url_prefix='/cat/<int:cat_id>/schedule')

schedule_schema = ScheduleSchema()
schedules_schema = ScheduleSchema(many=True)

@bp_sch.route('', methods=['GET'])
@jwt_required
def get_schedules(cat_id):

    user = user_loader()
    cat = Cat.query.get(cat_id)

    valid, msg = user_cat_validation(user, cat)

    if valid:
        return schedules_schema.jsonify(cat.schedules) 
    else:
        return jsonify(msg)


@bp_sch.route('', methods=['POST'])
@jwt_required
def add_schedule(cat_id):

    user = user_loader()
    cat = Cat.query.get(cat_id)

    valid, msg = user_cat_validation(user, cat)   
    
    if not valid:
        return jsonify(msg)
    
    params = request.get_json()

    time_start = params.get('time_start', None)
    time_end = params.get('time_end', None)
    food_ration = float(params.get('food_ration'))

    if not time_start or not time_end:
        return jsonify({"msg", "Parameter time_start or time_end cannot be empty."})

    new_sch = Schedule(
        datetime.strptime(time_start, '%H:%M:%S').time(),
        datetime.strptime(time_end, '%H:%M:%S').time(), 
        food_ration)
    cat.schedules.append(new_sch)

    db.session.add(new_sch)
    db.session.commit()

    return schedule_schema.jsonify(new_sch)

@bp_sch.route('/<int:sch_id>', methods=['GET'])
@jwt_required
def get_schedule(cat_id, sch_id):

    user = user_loader()
    cat = Cat.query.get(cat_id)
    schedule = Schedule.query.get(sch_id)

    valid, msg = user_cat_schedule_validation(user, cat, schedule)

    if not valid: return jsonify(msg)

    return schedule_schema.jsonify(schedule)


@bp_sch.route('/<int:sch_id>', methods=['PUT'])
@jwt_required
def update_schedule(cat_id, sch_id):

    user = user_loader()
    cat = Cat.query.get(cat_id)
    schedule = Schedule.query.get(sch_id)

    valid, msg = user_cat_schedule_validation(user, cat, schedule)

    if not valid: return jsonify(msg)

    params = request.get_json()
    
    time_start = params.get('time_start', None)
    time_end = params.get('time_end', None)
    food_ration = float(params.get('food_ration', schedule.food_ration))
    
    schedule.time_start = datetime.strptime(time_start, '%H:%M:%S').time() if time_start else schedule.time_start
    schedule.time_end = datetime.strptime(time_end, '%H:%M:%S').time() if time_end else schedule.time_end
    schedule.food_ration = food_ration

    db.session.commit()

    return schedule_schema.jsonify(schedule)


@bp_sch.route('/<int:id>', methods=['DELETE'])
@jwt_required
def delete_schedule(cat_id, sch_id):

    user = user_loader()
    cat = Cat.query.get(cat_id)
    schedule = Schedule.query.get(sch_id)

    valid, msg = user_cat_schedule_validation(user, cat, schedule)

    if not valid: return jsonify(msg)

    db.session.delete(schedule)
    db.session.commit()

    return schedule_schema.jsonify(schedule)

    
def user_cat_schedule_validation(user, cat, schedule):

    valid, msg = user_cat_validation(user, cat)

    if not valid: 
        return False, msg
    
    valid, msg = cat_schedule_validation(cat, schedule)

    if not valid: 
        return False, msg

    return True, {"msg": "Valid"}



def cat_schedule_validation(cat, schedule):

    if not schedule:
        return False, {"msg": "Schedule not found"}

    if not schedule.cat.id == cat.id:
        return False, {"msg": "This schedule is not belong to this cat"}
    
    return True, {"msg", "Valid"}
