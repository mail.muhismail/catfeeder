import os

from flask import Blueprint, request, jsonify, current_app
from flask_jwt_simple import jwt_required, get_jwt_identity
from werkzeug.utils import secure_filename
from cafe_api.extensions import db, jwt
from cafe_api.models import Cat
from cafe_api.schemas import CatSchema
from cafe_api.views.user import user_loader
from cafe_detection.catdetect import detect_save_cat, cat_training


bp_cat = Blueprint('cat', __name__, url_prefix='/cat')

cat_schema = CatSchema()
cats_schema = CatSchema(many=True)

ALLOWED_EXTENSIONS = set(['jpg', 'jpeg', 'png', 'mp4'])


@bp_cat.route('', methods=['GET'])
@jwt_required
def get_cat():

    user = user_loader()
    return cats_schema.jsonify(user.cats)


@bp_cat.route('', methods=['POST'])
@jwt_required
def add_cat():

    user = user_loader()

    name = request.json['name']
    food_ration = float(request.json['food_ration'])

    new_cat = Cat(name, food_ration)
    user.cats.append(new_cat)

    db.session.add(new_cat)
    db.session.commit()

    return cat_schema.jsonify(new_cat)


@bp_cat.route('/<int:id>', methods=['PUT'])
@jwt_required
def update_cat(id):

    user = user_loader()
    cat = Cat.query.get(id)

    valid, msg = user_cat_validation(user, cat)
    
    if not valid:
        return jsonify(msg)

    params = request.get_json()
    
    name = params.get('name', cat.name)
    food_ration = float(params.get('food_ration', cat.food_ration))
    
    cat.name = name
    cat.food_ration = food_ration

    db.session.commit()

    return cat_schema.jsonify(cat)

    
@bp_cat.route('/<int:id>', methods=['DELETE'])
@jwt_required
def delete_cat():

    user = user_loader()
    cat = cat.query.get(id)

    valid, msg = user_cat_validation(user, cat)
    
    if not valid:
        return jsonify(msg)

    db.session.delete(cat)
    db.session.commit()

    return cat_schema.jsonify(cat)


@bp_cat.route('/<int:id>/upload', methods=['POST'])
@jwt_required
def uploads_cat(id):

    # TODO: Check if the user already had recognizer file. If the user had it,
    #       delete the previous recognizer file after file successfully uploaded.

    user = user_loader()
    cat = Cat.query.get(id)

    valid, msg = user_cat_validation(user, cat)
    
    if not valid:
        return jsonify(msg)

    file = request.files['cat_image']

    if file.filename == '':
        return jsonify({'msg': 'Cannot upload empty file'})

    if file and allowed_file(file.filename):
        extension = secure_filename(file.filename).rsplit('.', 1)[1]
        media_type = 'video' if extension == 'mp4' else 'image'
        
        filename = '{}_{}_{}.{}'.format(
            user.id,
            str(id),
            latest_images_id(cat) + 1, 
            extension)

        cat_detect = detect_save_cat(
            os.path.join(current_app.config['UPLOAD_FOLDER'], filename), 
            file,
            media_type)

        if not cat_detect:
            return jsonify({'msg': 'No cat detected'})

        # file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

        return jsonify({'msg': 'Upload files success.'})
    
    return jsonify({'msg': 'Failed to upload files'})


@bp_cat.route('/train', methods=['GET'])
@jwt_required
def train_cat():

    # TODO: Check if the user already had recognizer file. Request for training
    #       recognizer will be rejected if the user already had the recognizer file.
    #       Why? To avoid server being 'bombarded' by this request which required
    #       alot of resource to be performed.
    
    user = user_loader()

    make_recognizer = cat_training(user.id)

    if not make_recognizer:
        return jsonify({'msg': 'Training failed'})
    
    return jsonify({'msg': 'Training success'})


def latest_images_id(cat):

    """ Give the latest number of image id for given cat.
    
    :param  cat:    Cat object to get the cat id

    :return         Integer of latest cat images id
    """
    
    list_files = os.listdir(current_app.config['UPLOAD_FOLDER'])

    filename = [files.split('.')[0] for files in list_files if os.path.isfile(os.path.join(current_app.config['UPLOAD_FOLDER'], files))]

    img_id = []
    for f in filename:
        # Get images of this cat
        # Filename: <user_id>_<cat_id>_<img_id>
        cat_id = int(f.split('_')[1])

        if cat_id == cat.id:
            img_id.append(int(f.split('_')[-1]))

    return max(img_id) if img_id else 0



def allowed_file(filename):
    
    """ Check if the filename has allowed extensions
    
    :param filename:    String of filename

    :return boolean
    """

    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def user_cat_validation(user, cat):
    
    """ Check if the user have that cat or that cat belong to this user. And 
    also check if the cat is exist.

    :param user:    User object that will be checked
    :param cat:     Cat object that will be checked

    :return validation_status, validation_message
    """

    if not cat:
        return False, {"msg": "Cat not found"}

    if not cat.user.id == user.id:
        return False, {"msg": "This is not your cat"}
    
    return True, {"msg", "Valid"}


