import os

from flask import Blueprint, request, jsonify, current_app
from flask_jwt_simple import jwt_required, create_jwt, get_jwt_identity
from sqlalchemy.exc import IntegrityError

from cafe_api.extensions import db, jwt, bcrypt
from cafe_api.schemas import UserSchema
from cafe_api.models import User

bp_user = Blueprint('user', __name__, url_prefix='/user')

user_schema = UserSchema()
users_schema = UserSchema(many=True)

@bp_user.route('', methods=['GET'])
@jwt_required
def get_user():

    user = user_loader()
    return user_schema.jsonify(user)


@bp_user.route('', methods=['POST'])
def add_user():

    params = request.get_json()
    username = params.get('username', None)
    password = params.get('password', None)
    first_name = params.get('first_name', None)
    last_name = params.get('last_name', None)

    if not username or not password or not first_name or not last_name:
        return jsonify({"msg": "All parameters cannot be empty"})

    new_user = User(username, password, first_name, last_name)

    try:
        db.session.add(new_user)
        db.session.commit()
    except IntegrityError:
        return jsonify({"msg": "Username '{}' has been used".format(username)})

    return user_schema.jsonify(new_user)


@bp_user.route('', methods=['PUT'])
@jwt_required
def update_user():
    
    """ Important: Change auth token after changed the username 
    """
    user = user_loader()

    params = request.get_json() 
    password = params.get('password', None)

    user.username = params.get('username', user.username)
    user.password = user.hash_password(password) if password else user.password
    user.first_name = params.get('first_name', user.first_name)
    user.last_name = params.get('last_name', user.last_name)
    user.device_id = params.get('device_id', user.device_id)
    
    try:
        db.session.commit()
    except IntegrityError:
        return jsonify({"msg": "Username '{}' has been used".format(username)})

    return user_schema.jsonify(user)


@bp_user.route('', methods=['DELETE'])
@jwt_required
def delete_user():

    user = user_loader()
    
    db.session.delete(user)
    db.session.commit()

    return user_schema.jsonify(user)


@bp_user.route('/login', methods=['POST'])
def login_user():
    
    if not request.is_json:
        return jsonify({"msg": "Only accept JSON in request"})

    params = request.get_json()
    username = params.get('username', None)
    password = params.get('password', None)

    if not username or not password:
        return jsonify({"msg": "Missing username or password parameter"})

    user = User.query.filter_by(username=username).first()

    if user is not None and user.check_password(password): 
        user.token = create_jwt(identity=username)
        return jsonify({'user_token': user.token}), 200
    
    return jsonify({"msg": "Wrong username or password"}), 401


@bp_user.route('/trainer/<device_id>', methods=['GET'])
def get_recognizer(device_id):

    user = User.query.filter_by(device_id=device_id).first()

    if user:
        recognizer_file = 'recognizer_{}.yml'.format(user.id)
        recognizer_path = os.path.join(current_app.config['PROJECT_ROOT'], 'recognizer', recognizer_file)
    
        with open(recognizer_path, 'rb') as f:
            recognizer_content = f.read()

    else:
        recognizer_content = False

    return recognizer_content

    



def user_loader():
    
    username = get_jwt_identity()
    user = User.query.filter_by(username=get_jwt_identity()).first()
    return user
