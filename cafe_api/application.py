from flask import Flask

from cafe_api.extensions import db, ma, jwt, bcrypt
from cafe_api.views import cat, user, schedule

def create_app(config_object=None):

    """ Create app based on given configuration 

    :param config_object:   The configuration object that will be used
    """

    app = Flask(__name__.split('.')[0])

    if config_object:
        app.config.from_object(config_object)

    # Extensions    
    db.init_app(app)
    ma.init_app(app)
    jwt.init_app(app)
    bcrypt.init_app(app)

    # Register Blueprints
    app.register_blueprint(cat.bp_cat)
    app.register_blueprint(user.bp_user)
    app.register_blueprint(schedule.bp_sch)

    return app