from cafe_api.application import ma
from cafe_api.models import Cat, User, Schedule

class UserSchema(ma.ModelSchema):
    
    class Meta:
        model = User
        fields = (
            'id', 
            'username', 
            'first_name', 
            'last_name', 
            'cats',
            'device_id')

class CatSchema(ma.ModelSchema):
    
    class Meta:
        model = Cat
        fields = (
            'id', 
            'name', 
            'food_ration')


class ScheduleSchema(ma.ModelSchema):

    class Meta:
        model = Schedule
        fields = (
            'id',
            'food_ration',
            'time_start',
            'time_end')
