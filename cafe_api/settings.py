""" Application configuration """
import os

class Config(object):

    """ Base Configuration """

    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    UPLOAD_FOLDER = os.path.join(PROJECT_ROOT, 'uploads')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevConfig(Config):
    
    DEBUG = True

    DB_NAME = 'cafe.db'
    DB_PATH = os.path.join(Config.PROJECT_ROOT, DB_NAME)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(DB_PATH)

    JWT_SECRET_KEY = 'super-duper-secret'